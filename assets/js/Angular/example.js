var app1 = angular.
    module('myApp', ['bootstrapLightbox']);


app1.controller('galleryController', function($scope,$http,Lightbox){

  //Gallery Start
  $scope.photos=[];
  $scope.galleries=[];
 
  $scope.loadGallery=function(){
        $http.get('http://www.sse.scienceinstituteonline.in/getWebGallery')
        //$http.get('http://localhost:8000/getWebGallery')
            .then(function (response) {
                $scope.galleries=response.data;
                $scope.photos=[];
                for(var i=0;i<response.data.length;i++){
                    for(var j=0;j<response.data[i].photos.length;j++){
                        $scope.photos.push({
                          'url':'http://www.sse.scienceinstituteonline.in/img/'+response.data[i].photos[j].photo,
                          'thumbUrl':'http://www.sse.scienceinstituteonline.in/img/'+response.data[i].photos[j].photo,
                          'caption':''
                        });
                    }
                }
                //console.log($scope.galleries);
            });
    };$scope.loadGallery();

      $scope.openLightboxModal = function (index) {
          Lightbox.openModal($scope.photos, index);
      };
    //GalleryEnd

    $scope.openFilter=function(index){
      $http.get('http://www.sse.scienceinstituteonline.in/getWebGallery')
            .then(function (response) {
                $scope.galleries=response.data;
                $scope.photos=[];
                for(var j=0;j<response.data[index].photos.length;j++){
                    $scope.photos.push({
                      'url':'http://www.sse.scienceinstituteonline.in/img/'+response.data[index].photos[j].photo,
                      'thumbUrl':'http://www.sse.scienceinstituteonline.in/img/'+response.data[index].photos[j].photo,
                      'caption':''
                    });
                }
                //console.log($scope.galleries);
            });
    }

});

