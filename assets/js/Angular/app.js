var app = angular.
    module('myApp', ['ui.bootstrap']);

app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);

app.controller('homeController', function($scope,$http){

    //Important Date Start
    $scope.importentDates=[];
    $scope.secondStart=0;
    $http.get('http://www.sse.scienceinstituteonline.in/getImportantDate')
        .then(function (response) {
            $scope.importentDates=response.data;
            $scope.secondStart=Math.round($scope.importentDates.length/2);
            //console.log(response.data);
        });
    //Important Date End

    //Recent News Start
    $scope.recentNews=[];
    $http.get('http://www.sse.scienceinstituteonline.in/getRecentNews')
        .then(function (response) {
            $scope.recentNews=response.data;
            //console.log(response.data);
        });
    //Recent News End


});

app.controller('answerkeyController', function($scope,$http){

   $scope.publish = false;
   $scope.rollno = '';
   $scope.mobile = '';
   $scope.showMessage='';
   $scope.loadSettings=function() {
        $http.get('http://www.sse.scienceinstituteonline.in/getAppSettings').
        //$http.get('http://localhost:8000/getAppSettings').
            success(function (data, status, headers, config) {
                if(data!=null) {
                    if (data[0].answerkey == '1')
                        $scope.publish = true;
                    else
                        $scope.publish = false;
                }

                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };
    $scope.loadSettings();

    //$scope.error=false;
    $scope.showProgress=false;
    $scope.getAnswerKey=function(){
        
        if($scope.rollno==''){
            alert('Roll Number required...');
            return 0;
        }

        if($scope.mobile==''){
            alert('Mobile Number required...');
            return 0;
        }
        $scope.showProgress=true;
        //$scope.showMessage='Please wait...Download in Progress...!!!';
        $http.get('http://www.sse.scienceinstituteonline.in/getAnswerKey',{params: {rollno:$scope.rollno,mobile:$scope.mobile,answerkeyfrom:'Online'},responseType: 'arraybuffer'}).
        //$http.get('http://localhost:8000/getAnswerKey',{params: {rollno:$scope.rollno,mobile:$scope.mobile},responseType: 'arraybuffer'}).
            success(function (data, status, headers, config) {
                   console.log(data);
                   
                   /*var file = new Blob([data], {type: 'application/pdf'});
                    var fileURL = URL.createObjectURL(file);
                    window.open(fileURL);
                    */
                    var arr = data;
                    var byteArray = new Uint8Array(arr);
                    var a = window.document.createElement('a');

                    a.href = window.URL.createObjectURL(
                        new Blob([byteArray], { type: 'application/octet-stream' })
                    );
                    a.download ='answerkey.pdf' ;//headers('filename');

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();

                    $scope.showMessage='';
                    // Remove anchor from body
                    document.body.removeChild(a);
                    $scope.showProgress=false;
                   //$scope.error=false;
            }).error(function (data, status, headers, config) {
                console.log(data);
                $scope.showProgress=false;
                //$scope.error=true;
                $scope.showMessage='Invalid Roll Number or Mobile Number';
            });

        };
});



app.controller('examinationController', function($scope,$http){

    //Exam Pattern Start
    $scope.exampatterns=[];
    $http.get('http://www.sse.scienceinstituteonline.in/getExamPattern')
        .then(function (response) {
            $scope.exampatterns=response.data;
            //console.log(response.data);
        });
    //Exam Pattern End

    //Question Paper Start
    $scope.questions=[];
    $scope.secondStart=0;
    $http.get('http://www.sse.scienceinstituteonline.in/getquestion')
        .then(function (response) {
            $scope.questions=response.data;
            $scope.secondStart=Math.round($scope.questions.length/2);
            //console.log(response.data);
        });
    //Question Paper END

    //Centres Start
    $scope.centres=[];
    $scope.examSecond=0;
    //$scope.examThird=0;
    $http.get('http://www.sse.scienceinstituteonline.in/getCentres')
        .then(function (response) {
            $scope.centres=response.data;
            $scope.examSecond=Math.round($scope.centres.length/2);
            //console.log(response.data);
        });
    //Centres END

});


app.controller('onlineregistrationController', function($scope,$http,$anchorScroll){

    $scope.newregistration = {};
    $scope.examcenters = [];
    $scope.date = [];
    $scope.printDiv = false;
    $scope.publish=false;
    $scope.editForm=false;
    $scope.accept=false;
    $scope.completed=false;
    $scope.loadSpinner=false;
    $scope.applicationform = {};
    $scope.student={};

    $scope.gotAccept=function(){
        $scope.accept=true;
        $scope.showForm=true;
        $anchorScroll();
    };

    $scope.loadSettings=function() {
        $http.get('http://www.sse.scienceinstituteonline.in/getAppSettings').
        //$http.get('http://localhost:8000/getAppSettings').
            success(function (data, status, headers, config) {
                if(data!=null) {
                    if (data[0].application == '1')
                        $scope.publish = true;
                    else
                        $scope.publish = false;
                }

                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };
    $scope.loadSettings();

    $scope.loadExamCentres=function(boardofexam){
        $http.get('http://www.sse.scienceinstituteonline.in/getAppExamCentre',{params:{all:'All',boardofexam:boardofexam}}).
        //$http.get('http://localhost:8000/getAppExamCentre').
            success(function (data, status, headers, config) {
                $scope.examcenters=data;
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    $scope.firstSubmit=function(){
        $scope.birth = $scope.date.day+"/"+$scope.date.month+"/"+$scope.date.year;
        $scope.option1='';
        $scope.option2='';
        for (var i = 0; i <$scope.examcenters.length; i++) {
            if($scope.examcenters[i].id==$scope.newregistration.exam_centres_id1)
                $scope.option1=$scope.examcenters[i].name;
            if($scope.examcenters[i].id==$scope.newregistration.exam_centres_id2)
                $scope.option2=$scope.examcenters[i].name;
        }
        $scope.editForm=true;
        $scope.showForm=false;

        $anchorScroll();
    }

    $scope.editApplication=function(){
        $scope.editForm=false;
        $scope.showForm=true;
    }
    $scope.disable=false;
    $scope.addRegistration = function () {
        
        if($scope.reject==false) {
            $scope.loadSpinner=true;
            $scope.disable=true;
            var dateofbirth = $scope.date.year + "/" + $scope.date.month + "/" + $scope.date.day;
            $http.get('http://www.sse.scienceinstituteonline.in/registerWebOnline', {
            //$http.get('http://localhost:8000/registerWebOnline', {
                params: {
                    name: $scope.newregistration.name.toUpperCase(),
                    parentname: $scope.newregistration.parentname.toUpperCase(),
                    gender: $scope.newregistration.gender,
                    dateofbirth: dateofbirth,
                    parentmobile: $scope.newregistration.parentmobile,
                    landphone: $scope.newregistration.landphone,
                    schoolname: $scope.newregistration.schoolname.toUpperCase(),
                    address1: $scope.newregistration.address1.toUpperCase(),
                    district: $scope.newregistration.district,
                    pin: $scope.newregistration.pin,
                    email: $scope.newregistration.email,
                    knownabout: $scope.newregistration.knownabout,
                    modeofpayment: $scope.newregistration.modeofpayment,
                    boardofexam: $scope.newregistration.boardofexam,
                    medium: $scope.newregistration.medium,
                    exam_centres_id1: $scope.newregistration.exam_centres_id1,
                    exam_centres_id2: $scope.newregistration.exam_centres_id2,
                    through: 'Online',
                    status: 'Pending'

                }
            }).
                success(function (data, status, headers, config) {
                    console.log(data);
                    console.log('success');
                    $scope.loadSpinner=false;
                    $scope.applicationform = data;
                    $scope.printDiv = true;
                    $scope.completed = true;
                    $scope.student = {};
                    $anchorScroll();
                }).error(function (data, status, headers, config) {
                    console.log(data);
                    alert('Try again later');
                });
        }else{
            alert('You already registered with this Parent/Guardian Mobile Number.Try to change this Mobile Number: '+$scope.newregistration.parentmobile);
        }
    };


    $scope.checkPhoneExsist=function(phone){
        $http.get('http://www.sse.scienceinstituteonline.in/checkexsist',{params:{phone:phone}}).
        //$http.get('http://localhost:8000/checkexsist',{params:{phone:phone}}).
            success(function (data, status, headers, config) {
                $scope.student=data;
                $scope.reject=true;
                if(data.OK=='Ready')
                    $scope.reject=false;
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

});


/**
 * Created by Shahid on 21/8/16.
 */
app.controller('onlinehallticketController', function ($scope, $http,$window) {

    $scope.students=[];
    $scope.student={};
    $scope.showdetails=false;
    $scope.publish=false;
    $scope.date = [];

    $http.get('http://www.sse.scienceinstituteonline.in/getAppSettings').
    //$http.get('http://localhost:8000/getAppSettings').
        success(function (data, status, headers, config) {
            if(data[0].hallticket=='1')
                $scope.publish=true;
            else
                $scope.publish=false;

            //console.log(data);
        }).error(function (data, status, headers, config) {
            console.log(data);
        });

    //$http.get('http://www.sse.scienceinstituteonline.in/getApplicationList',{params:{'status':'Approved'}})
   /* $http.get('http://localhost:8000/getApplicationList',{params:{'status':'Approved'}})
        .then(function(response){
            $scope.students=response.data;
        });*/


    $scope.getDetails=function(student){
        $scope.student=student;
        $scope.showdetails=true;
    };

    $scope.getName=function(name){
        $http.get('http://www.sse.scienceinstituteonline.in/getNameList',{params:{'name':name}})
        //$http.get('http://localhost:8000/getNameList',{params:{'name':name}})
        .then(function(response){
            $scope.students=response.data;
        });
    };
    

    $scope.instruction1='';
    $scope.instruction2='';

    $scope.getHallTicket=function(){
        if($scope.date.year==null || $scope.date.month==null || $scope.date.day==null) {
            alert('Invalid Date of Birth');
            return 0;
        }
        var dateofbirth = $scope.date.year + "/" + $scope.date.month + "/" + $scope.date.day;

        $scope.hallticket={};
        $http.get('http://www.sse.scienceinstituteonline.in/gethallticket',{params:{'date':dateofbirth,'id':$scope.student.id,'exam_centres_id':$scope.student.exam_centres_id,from:'Online'}})
        //$http.get('http://localhost:8000/gethallticket',{params:{'date':dateofbirth,'id':$scope.student.id,'exam_centres_id':$scope.student.exam_centres_id,from:'Not Control'}})
            .success(function (data, status, headers, config) {
                $scope.hallticket=data;
                 //$window.open('http://192.168.10.137:8000/downloadHallticketOnline/'+$scope.student.id, '_blank');
                $window.location.href='http://www.sse.scienceinstituteonline.in/downloadHallticketOnline/'+$scope.student.id;
                console.log($scope.hallticket);
                $scope.error='';
                //console.log(data);
            }).error(function (data, status, headers, config) {
                $scope.error=data.error;
                console.log(data);
            });
    };    

});

app.controller('onlineresultController', function($scope,$http){

    $scope.result = [];
    $scope.publish=false;
    $scope.errors={};
    $scope.showerror=false;
    $scope.hallticketno='';
    $scope.hideResult=true;

    $scope.year=new Date();

    //$http.get('http://localhost:8000/getAppSettings').
    $http.get('http://www.sse.scienceinstituteonline.in/getAppSettings').
        success(function (data, status, headers, config) {
            if(data[0].result=='1')
                $scope.publish=true;
            else
                $scope.publish=false;

            console.log(data);
        }).error(function (data, status, headers, config) {
            console.log(data);
        });

    $scope.getResult=function(){
        //$http.get('http://localhost:8000/getresult',{params:{hallticketno:$scope.hallticketno}}).
        $http.get('http://www.sse.scienceinstituteonline.in/getresult',{params:{hallticketno:$scope.hallticketno}}).
            success(function (data, status, headers, config) {
                $scope.result=data;
                $scope.errors={};
                $scope.showerror=false;
                $scope.hideResult=false;
                console.log(data);
            }).error(function (data, status, headers, config) {
                $scope.errors=data;
                $scope.showerror=true;
                console.log(data);
            });
    }

    $scope.showProgress=false;
    $scope.print=function(id){

        $scope.showProgress=true;
        $http.get('http://www.sse.scienceinstituteonline.in/printResult',{params: {id:id},responseType: 'arraybuffer'}).
        //$http.get('http://localhost:8000/printResult',{params: {id:id},responseType: 'arraybuffer'}).
            success(function (data, status, headers, config) {
                   console.log(data);
                   
                   
                    var arr = data;
                    var byteArray = new Uint8Array(arr);
                    var a = window.document.createElement('a');

                    a.href = window.URL.createObjectURL(
                        new Blob([byteArray], { type: 'application/octet-stream' })
                    );
                    a.download ='result.pdf' ;//headers('filename');

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();

                    $scope.showMessage='';
                    // Remove anchor from body
                    document.body.removeChild(a);
                   //$scope.error=false;

                   $scope.showProgress=false;
            }).error(function (data, status, headers, config) {
                console.log(data);
                //$scope.error=true;
                
            });
    }
});

app.controller('downloadController', function($scope,$http,$window){
    $scope.showerror=false;
    $scope.getApplication=function(){
        var dateofbirth = $scope.date.year + "-" + $scope.date.month + "-" + $scope.date.day;
        $http.get('http://www.sse.scienceinstituteonline.in/getApplicationDownload',{params:{parentmobile:$scope.parentmobile,dateofbirth:dateofbirth}}).
            success(function (data, status, headers, config) {
                $window.location.href='http://www.sse.scienceinstituteonline.in/downloadApplicationOnline/'+data.id;
                console.log(data);
            }).error(function (data, status, headers, config) {
                $scope.showerror=true;
                console.log(data);
            });
    }
});

app.controller('feedbackController', function($scope,$http){

    $scope.sendFeedback=function(){
        $http.get('http://www.sse.scienceinstituteonline.in/sendFeedback',{params:{name:$scope.name,email:$scope.email,message:$scope.message}}).
                success(function (data, status, headers, config) {
                    alert('Message Send...!');
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
    }
});
