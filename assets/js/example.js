var app1 = angular.
    module('myApp', ['bootstrapLightbox']);


app1.controller('galleryController', function($scope,$http,Lightbox){

  //Gallery Start
  $scope.photos=[];
  $scope.galleries=[];
  //$http.get('http://www.sse.scienceinstituteonline.in/getWebGallery')
  $http.get('http://localhost:8000/getWebGallery')
      .then(function (response) {
          $scope.galleries=response.data;
          $scope.photos=[];
          for(var i=0;i<response.data.length;i++){
              for(var j=0;j<response.data[i].photos.length;j++){
                  $scope.photos.push({
                    'url':'http://localhost:8000/img/'+response.data[i].photos[j].photo,
                    'thumbUrl':'http://localhost:8000/img/'+response.data[i].photos[j].photo,
                    'caption':''
                  });
              }
          }
          console.log($scope.galleries);
      });

      $scope.openLightboxModal = function (index) {
          Lightbox.openModal($scope.photos, index);
      };

      $scope.changeCategory=function (index) {
         // if(index=='-1'){
         //      $http.get('http://localhost:8000/getWebGallery')
         //      .then(function (response) {
         //              $scope.galleries=response.data;
         //              $scope.photos=[];
         //              for(var i=0;i<response.data.length;i++){
         //                  for(var j=0;j<response.data[i].photos.length;j++){
         //                      $scope.photos.push({
         //                        'url':'http://localhost:8000/img/'+response.data[i].photos[j].photo,
         //                        'thumbUrl':'http://localhost:8000/img/'+response.data[i].photos[j].photo,
         //                        'caption':''
         //                      });
         //                  }
         //              }
         //              //console.log($scope.galleries);
         //      });
         // }else{
         //    $http.get('http://localhost:8000/getWebGallery')
         //      .then(function (response) {
         //              $scope.galleries=response.data;
         //              $scope.photos=[];
         //                  for(var j=0;j<response.data[index].photos.length;j++){
         //                      $scope.photos.push({
         //                        'url':'http://localhost:8000/img/'+response.data[index].photos[j].photo,
         //                        'thumbUrl':'http://localhost:8000/img/'+response.data[index].photos[j].photo,
         //                        'caption':''
         //                      });
         //                  }
         //      });
         // }
         alert('Hai');
      };
    //GalleryEnd

});



// angular.module('demo1', ['bootstrapLightbox']);

// angular.module('demo1').controller('GalleryCtrl', function ($scope, Lightbox) {
//   $scope.images = [
//     {
//       'url': 'https://farm6.staticflickr.com/5830/20552523531_e1efec8d49_k.jpg',
//       'thumbUrl': 'https://farm6.staticflickr.com/5830/20552523531_ef720cd2f1_s.jpg',
//       'caption': 'This image has dimensions 2048x1519 and the img element is scaled to fit inside the window.'
//     },
//     {
//       'url': 'https://farm8.staticflickr.com/7300/12807911134_ff56d1fb3b_b.jpg',
//       'thumbUrl': 'https://farm8.staticflickr.com/7300/12807911134_ff56d1fb3b_s.jpg'
//     },
//     {
//       'url': 'https://farm1.staticflickr.com/400/20228789791_52fb84917f_b.jpg',
//       'thumbUrl': 'https://farm1.staticflickr.com/400/20228789791_52fb84917f_s.jpg',
//       'caption': 'The left and right arrow keys are binded for navigation. The escape key for closing the modal is binded by AngularUI Bootstrap.'
//     },
//     {
//       'url': 'https://farm1.staticflickr.com/260/20185156095_912c2714ef_b.jpg',
//       'thumbUrl': 'https://farm1.staticflickr.com/260/20185156095_912c2714ef_s.jpg'
//     },
//     {
//       'url': 'https://farm6.staticflickr.com/5757/20359334789_57316968ed_m.jpg',
//       'thumbUrl': 'https://farm6.staticflickr.com/5757/20359334789_57316968ed_s.jpg',
//       'caption': 'Default minimum modal dimensions (400x200) apply for this image (240x95).'
//     },
//     {
//       'url': 'https://farm1.staticflickr.com/359/18741723375_28c89372d7_c.jpg',
//       'thumbUrl': 'https://farm1.staticflickr.com/359/18741723375_28c89372d7_s.jpg'
//     },
//     {
//       'url': 'https://farm6.staticflickr.com/5606/15425945368_6f6ae945fc.jpg',
//       'thumbUrl': 'https://farm6.staticflickr.com/5606/15425945368_6f6ae945fc_s.jpg'
//     },
//     ];

//   $scope.openLightboxModal = function (index) {
//     Lightbox.openModal($scope.images, index);
//   };
    
// });